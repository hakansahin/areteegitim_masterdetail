jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

// We cannot provide stable mock data out of the template.
// If you introduce mock data, by adding .json files in your webapp/localService/mockdata folder you have to provide the following minimum data:
// * At least 3 SoheaderdataSet in the list
// * All 3 SoheaderdataSet have at least one SOHeaderTOItems

sap.ui.require([
	"sap/ui/test/Opa5",
	"egitim/so/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"egitim/so/test/integration/pages/App",
	"egitim/so/test/integration/pages/Browser",
	"egitim/so/test/integration/pages/Master",
	"egitim/so/test/integration/pages/Detail",
	"egitim/so/test/integration/pages/NotFound"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "egitim.so.view."
	});

	sap.ui.require([
		"egitim/so/test/integration/MasterJourney",
		"egitim/so/test/integration/NavigationJourney",
		"egitim/so/test/integration/NotFoundJourney",
		"egitim/so/test/integration/BusyJourney",
		"egitim/so/test/integration/FLPIntegrationJourney"
	], function () {
		QUnit.start();
	});
});