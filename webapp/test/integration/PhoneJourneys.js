jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

sap.ui.require([
	"sap/ui/test/Opa5",
	"egitim/so/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"egitim/so/test/integration/pages/App",
	"egitim/so/test/integration/pages/Browser",
	"egitim/so/test/integration/pages/Master",
	"egitim/so/test/integration/pages/Detail",
	"egitim/so/test/integration/pages/NotFound"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "egitim.so.view."
	});

	sap.ui.require([
		"egitim/so/test/integration/NavigationJourneyPhone",
		"egitim/so/test/integration/NotFoundJourneyPhone",
		"egitim/so/test/integration/BusyJourneyPhone"
	], function () {
		QUnit.start();
	});
});